# Darcy Simulations Data

This repository contains the illustration Figure 1 and the data to generate Figures 2-10, D1 and D2  of the paper "Mechanisms, upscaling and prediction of anomalous dispersion in heterogeneous porous media" by Alessandro Comolli, Vivien Hakoun and Marco Dentz